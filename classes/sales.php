<?php
class Sales
{
    private $table = "suppliers";
    private $columns = ['id','first_name','last_name','gst_no','phone_no','email_id','company_name','created_at','updated_at'];
    protected $id;
    private $database;
    private $validator;
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }
    public function addSales($data)
    {
        //INSERT DATA IN DATABASE
        try{
            $this->database->beginTransaction();
            $invoice_id = $this->di->get('database')->insert("invoice",["customer_id"=>$data["customer_id"]]);
            // Util::dd($invoice_id);
            $sales_data_to_be_inserted["invoice_id"] = $invoice_id;
            for($i=0 ; $i<count($data["product_id"]);$i++){
                $sales_data_to_be_inserted["product_id"] = $data["product_id"][$i];
                $sales_data_to_be_inserted["quantity"] = $data["quantity"][$i];
                $sales_data_to_be_inserted["discount"] = $data["discount"][$i];
                $this->di->get('database')->insert("sales",$sales_data_to_be_inserted);
            }
            $this->database->commit();
            return $invoice_id;
        }catch(Exception $e){
            $this->database->rollBack();
            return ADD_ERROR;
        }
        return VALIDATION_ERROR;
    }
}