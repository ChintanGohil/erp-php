<?php
class Supplier
{
    private $table = "suppliers";
    private $columns = ['id','first_name','last_name','gst_no','phone_no','email_id','company_name','created_at','updated_at'];
    protected $id;
    private $database;
    private $validator;
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }
    public function getValidator()
    {
        return $this->validator;
    }
    public function validateData($data,$id="")
	{
		$this->validator = $this->di->get('validator');
		$this->validator = $this->validator->check($data,[
			'first_name'=>[
				'required'=>true,
				'minlength'=>3,
				'maxlength'=>28,
			],
			'last_name'=>[
				'required'=>true,
				'minlength'=>3,
				'maxlength'=>28,
			],
			'gst_no'=>[
				'required'=>true,
				'unique'=>$this->table.".".$id
			],
			'phone_no'=>[
				'required'=>true,
				'unique'=>$this->table.".".$id
			],
			'email_id'=>[
				'required'=>true,
				'email'=>true,
				'unique'=>$this->table.".".$id
			],
			'company_name'=>[
				'required'=>true,
			],
			'block_no'=>[
				'required'=>true,
			],
			'street'=>[
				'required'=>true,
			],
			'city'=>[
				'required'=>true,
			],
			'pincode'=>[
				'required'=>true,
			],
			'state'=>[
				'required'=>true,
			],
			'country'=>[
				'required'=>true,
			],
			'town'=>[
				'required'=>true,
			]
		]);
	}
    public function addSupplier($data)
    {
        //VALIDATE DATA
        $this->validateData($data);

        //INSERT DATA IN DATABASE
        if(!$this->validator->fails())
        {
            try{
                $this->database->beginTransaction();
                $data_to_be_inserted = ['first_name'=>$data['first_name'],
                'last_name'=>$data['last_name'],
                'gst_no'=>$data['gst_no'],
                'phone_no'=>$data['phone_no'],
                'email_id'=>$data['email_id'],
                'company_name'=>$data['company_name']];
                $category_id = $this->database->insert($this->table, $data_to_be_inserted);
                $this->database->commit();
                return ADD_SUCCESS;
            }catch(Exception $e){
                $this->database->rollBack();
                return ADD_ERROR;
            }
        }
        return VALIDATION_ERROR;
    }
    public function update($data,$id){
        // Util::dd($data);
        $this->validateData($data,$id);
		if(!$this->validator->fails())
		{
			try
			{
				$this->database->beginTransaction();
				$supplier_data_to_be_inserted=[];
				$address_data_to_be_inserted=[];
				
				$old = $this->getSupplierById($data["supplier_id"]);
                // Util::dd($old);
                // Util::dd($old->first_name);
				if($old->first_name != $data["first_name"]){
					$supplier_data_to_be_inserted["first_name"] = $data["first_name"]; 
				}
				if($old->last_name != $data["last_name"]){
					$supplier_data_to_be_inserted["last_name"] = $data["last_name"]; 
				}
				if($old->gst_no != $data["gst_no"]){
					$supplier_data_to_be_inserted["gst_no"] = $data["gst_no"]; 
				}
				if($old->phone_no != $data["phone_no"]){
					$supplier_data_to_be_inserted["phone_no"] = $data["phone_no"]; 
				}
				if($old->email_id != $data["email_id"]){
					$supplier_data_to_be_inserted["email_id"] = $data["email_id"]; 
				}
				if($old->company_name != $data["company_name"]){
					$supplier_data_to_be_inserted["company_name"] = $data["company_name"]; 
				}

				if($old->block_no != $data["block_no"]){
					$address_data_to_be_inserted["block_no"] = $data["block_no"]; 
				}
				if($old->street != $data["street"]){
					$address_data_to_be_inserted["street"] = $data["street"]; 
				}
				if($old->city != $data["city"]){
					$address_data_to_be_inserted["city"] = $data["city"]; 
				}
				if($old->pincode != $data["pincode"]){
					$address_data_to_be_inserted["pincode"] = $data["pincode"]; 
				}
				if($old->state != $data["state"]){
					$address_data_to_be_inserted["state"] = $data["state"]; 
				}
				if($old->country != $data["country"]){
					$address_data_to_be_inserted["country"] = $data["country"]; 
				}
				if($old->town != $data["town"]){
					$address_data_to_be_inserted["town"] = $data["town"]; 
				}
				// Util::dd(count($address_data_to_be_inserted));
				$address = $this->getAddressBysupplierId($data["supplier_id"]);
				// Util::dd($address);
				if(count($address_data_to_be_inserted)>0){
                    // Util::dd($address);
					$this->di->get("database")->update("address",$address_data_to_be_inserted,"id=".$address->id);
				}
				if(count($supplier_data_to_be_inserted)>0){
					$this->di->get("database")->update("suppliers",$supplier_data_to_be_inserted,"id=".$data["suppier_id"]);
				}
				$this->database->commit();
				return UPDATE_SUCCESS;
			}catch(Exception $e){
                Util::dd("exception");
				$this->database->rollBack();
				return UPDATE_ERROR;
			}
		}else{
			Util::dd($this->getValidator()->errors()->all());
			return VALIDATION_ERROR;
		}
	}
    public function getAddressBySupplierId($id){
		$query = "select address.* from suppliers INNER JOIN address_supplier INNER JOIN address ON suppliers.id = address_supplier.supplier_id and address_supplier.address_id=address.id and suppliers.id=".$id;
		$result = $this->di->get("database")->raw($query);
		return $result[0];
	}
	public function getSupplierById($id){
		// $result = $this->di->get("database")->readData("customers",[],"id=".$id);
		$query = "select address.*,suppliers.* from suppliers INNER JOIN address_supplier INNER JOIN address ON suppliers.id = address_supplier.supplier_id and address_supplier.address_id=address.id and suppliers.id=".$id;
		$result = $this->di->get("database")->raw($query);
		// Util::dd($result);
		return $result[0];
	}
    public function getJSONDataForDataTable($draw,$search_parameter,$order_by,$start,$length)
    {
        $query = "select CONCAT(block_no, \", \", street, \", \", city, \" - \", pincode, \".\" , \" \", state, \".\", \" \", country, \".\", \" \", town) as full_address,suppliers.* from address INNER JOIN address_supplier inner join suppliers on address.id=address_supplier.address_id and address_supplier.supplier_id = suppliers.id and address.id in (SELECT address_supplier.address_id from address_supplier INNER JOIN suppliers ON suppliers.id=address_supplier.supplier_id) and suppliers.deleted=0";
        $totalRowCountQuery = "SELECT count(*) as total_count from address INNER JOIN address_supplier inner join suppliers on address.id=address_supplier.address_id and address_supplier.supplier_id = suppliers.id and address.id in (SELECT address_supplier.address_id from address_supplier INNER JOIN suppliers ON suppliers.id=address_supplier.supplier_id) and suppliers.deleted=0";
        $filteredRowCountQuery = "SELECT count(*) as total_count from address INNER JOIN address_supplier inner join suppliers on address.id=address_supplier.address_id and address_supplier.supplier_id = suppliers.id and address.id in (SELECT address_supplier.address_id from address_supplier INNER JOIN suppliers ON suppliers.id=address_supplier.supplier_id) and suppliers.deleted=0";
        
        if($search_parameter != null)
        {
            $query .= " AND first_name LIKE '%{$search_parameter}%' 
            OR last_name LIKE '%{$search_parameter}%'
            OR gst_no LIKE '%{$search_parameter}%'
            OR phone_no LIKE '%{$search_parameter}%'
            OR email_id LIKE '%{$search_parameter}%'
            OR company_name LIKE '%{$search_parameter}%'
            OR created_at LIKE '%{$search_parameter}%'
            OR updated_at LIKE '%{$search_parameter}%'
            ";
            $filteredRowCountQuery .= " AND first_name LIKE '%{$search_parameter}%' 
            OR last_name LIKE '%{$search_parameter}%'
            OR gst_no LIKE '%{$search_parameter}%'
            OR phone_no LIKE '%{$search_parameter}%'
            OR email_id LIKE '%{$search_parameter}%'
            OR company_name LIKE '%{$search_parameter}%'
            OR created_at LIKE '%{$search_parameter}%'
            OR updated_at LIKE '%{$search_parameter}%'";
        }
        //Util::dd($this->columns[$order_by[0]['column']]));
        if($order_by != null)
        {
            $query .= " ORDER BY {$this->columns[$order_by[0]['column']]} {$order_by[0]['dir']}";
            $filteredRowCountQuery .= " ORDER BY {$this->columns[$order_by[0]['column']]} {$order_by[0]['dir']}";
        }
        else{
            $query .=" ORDER BY {$this->columns[0]} ASC";
            $filteredRowCountQuery .=" ORDER BY {$this->columns[0]} ASC";
        }

        if($length != -1)
        {
            $query .= " LIMIT {$start}, {$length}";
        }

        $totalRowCountResult = $this->database->raw($totalRowCountQuery);
        $numberOfTotalRows = is_array($totalRowCountResult) ? $totalRowCountResult[0]->total_count : 0;

        $filteredRowCountResult =  $this->database->raw($filteredRowCountQuery);
        $numberOfFilteredRows = is_array($filteredRowCountResult) ? $filteredRowCountResult[0]->total_count : 0;

        $fetchedData = $this->database->raw($query);
        $data = [];
        $numRows = is_array($fetchedData) ? count($fetchedData) : 0;
        for($i=0; $i<$numRows; $i++)
        {
            $subArray = [];
            $subArray[] = $start+$i+1; 
            $subArray[] = $fetchedData[$i]->first_name;
            $subArray[] = $fetchedData[$i]->last_name;
            $subArray[] = $fetchedData[$i]->gst_no;
            $subArray[] = $fetchedData[$i]->phone_no;
            $subArray[] = $fetchedData[$i]->email_id;
            $subArray[] = $fetchedData[$i]->company_name;
            $subArray[] = $fetchedData[$i]->created_at;
            $subArray[] = $fetchedData[$i]->updated_at;
            $subArray[] = $fetchedData[$i]->full_address;
            $subArray[] = <<<BUTTONS
            <a href='edit-supplier.php?id={$fetchedData[$i]->id}'><button class='btn btn-outline-primary btn-sm edit' data-id='{$fetchedData[$i]->id}' data-toggle="modal" data-target="#editModal"><i class="fas fa-pencil-alt"></i></button></a>
            <button class='btn btn-outline-danger btn-sm delete' data-id='{$fetchedData[$i]->id}' data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash-alt"></i></button>
BUTTONS;

            $data[] = $subArray;
        }

        $output = array(
            'draw'=>$draw,
            'recordsTotal'=>$numberOfTotalRows,
            'recordsFiltered'=>$numberOfFilteredRows,
            'data'=>$data
        );
        echo json_encode($output);
    }
    public function getCategoryByID($id,$fetchMode = PDO::FETCH_OBJ)
    {
        $query = "SELECT * FROM {$this->table} WHERE id = {$id} AND deleted = 0";
        $result = $this->database->raw($query,$fetchMode);
        return $result;
    }
    // public function update($data,$id)
    // {
    //     $data_to_be_updated = ['name' => $data['customer_name']];
    //     $this->validateData($data_to_be_updated);
    //     if(!$this->validator->fails())
    //     {
    //         try{
    //             $this->database->beginTransaction();
    //             $result = $this->database->update($this->table,$data_to_be_updated,"id = {$id}");
    //             $this->database->commit();
    //             return UPDATE_SUCCESS;
    //         }catch(Exception $e){
    //             $this->database->rollBack();
    //             return UPDATE_ERROR;
    //         }
    //     }
    //     else{
    //         return VALIDATION_ERROR;
    //     }
    // }
    public function delete($id)
    {
        try{
            $this->database->beginTransaction();
			$this->database->delete($this->table, "id = {$id}");

			$address = $this->getAddressBySupplierId($id);
			// Util::dd($address->id);
			$this->database->delete("address", "id = {$address->id}");
			$this->database->commit();
            return DELETE_SUCCESS;
        }catch(Exception $e){
            $this->database->rollBack();
            return DELETE_ERROR;
        }
    }
}