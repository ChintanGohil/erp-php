<?php
class Customer
{
	private $table = "customers";
	private $columns = ['id','first_name','last_name','gst_no','phone_no','email','gender','created_at','updated_at'];
	protected $di;
	private $database;
	private $validator;
	public function __construct(DependencyInjector $di){
		$this->di = $di;
		$this->database = $this->di->get('database');
	}
	public function getValidator()
	{
		return $this->validator;
	}
	public function validateData($data,$id="")
	{
		$this->validator = $this->di->get('validator');
		$this->validator = $this->validator->check($data,[
			'first_name'=>[
				'required'=>true,
				'minlength'=>3,
				'maxlength'=>28,
			],
			'last_name'=>[
				'required'=>true,
				'minlength'=>3,
				'maxlength'=>28,
			],
			'gst_no'=>[
				'required'=>true,
				'unique'=>$this->table.".".$id
			],
			'phone_no'=>[
				'required'=>true,
				'unique'=>$this->table.".".$id
			],
			'email_id'=>[
				'required'=>true,
				'email'=>true,
				'unique'=>$this->table.".".$id
			],
			'gender'=>[
				'required'=>true,
			],
			'block_no'=>[
				'required'=>true,
			],
			'street'=>[
				'required'=>true,
			],
			'city'=>[
				'required'=>true,
			],
			'pincode'=>[
				'required'=>true,
			],
			'state'=>[
				'required'=>true,
			],
			'country'=>[
				'required'=>true,
			],
			'town'=>[
				'required'=>true,
			]
		]);
	}
	public function update($data,$id){
		$this->validateData($data,$id);
		if(!$this->validator->fails())
		{
			try
			{
				$this->database->beginTransaction();
				$customer_data_to_be_inserted=[];
				$address_data_to_be_inserted=[];
				// Util::dd($old);
				$old = $this->getCustomerById($data["cust_id"]);
				// Util::dd($old->first_name);
				if($old->first_name != $data["first_name"]){
					$customer_data_to_be_inserted["first_name"] = $data["first_name"]; 
				}
				if($old->last_name != $data["last_name"]){
					$customer_data_to_be_inserted["last_name"] = $data["last_name"]; 
				}
				if($old->gst_no != $data["gst_no"]){
					$customer_data_to_be_inserted["gst_no"] = $data["gst_no"]; 
				}
				if($old->phone_no != $data["phone_no"]){
					$customer_data_to_be_inserted["phone_no"] = $data["phone_no"]; 
				}
				if($old->email_id != $data["email_id"]){
					$customer_data_to_be_inserted["email_id"] = $data["email_id"]; 
				}
				if($old->gender != $data["gender"]){
					$customer_data_to_be_inserted["gender"] = $data["gender"]; 
				}

				if($old->block_no != $data["block_no"]){
					$address_data_to_be_inserted["block_no"] = $data["block_no"]; 
				}
				if($old->street != $data["street"]){
					$address_data_to_be_inserted["street"] = $data["street"]; 
				}
				if($old->city != $data["city"]){
					$address_data_to_be_inserted["city"] = $data["city"]; 
				}
				if($old->pincode != $data["pincode"]){
					$address_data_to_be_inserted["pincode"] = $data["pincode"]; 
				}
				if($old->state != $data["state"]){
					$address_data_to_be_inserted["state"] = $data["state"]; 
				}
				if($old->country != $data["country"]){
					$address_data_to_be_inserted["country"] = $data["country"]; 
				}
				if($old->town != $data["town"]){
					$address_data_to_be_inserted["town"] = $data["town"]; 
				}
				// Util::dd(count($address_data_to_be_inserted));
				$address = $this->getAddressByCustomerId($data["cust_id"]);
				// Util::dd($address);
				if(count($address_data_to_be_inserted)>1){
					$this->di->get("database")->update("address",$address_data_to_be_inserted,"id=".$address->id);
				}
				if(count($customer_data_to_be_inserted)>1){
					$this->di->get("database")->update("customers",$customer_data_to_be_inserted,"id=".$data["cust_id"]);
				}
				$this->database->commit();
				return UPDATE_SUCCESS;
			}catch(Exception $e){
				$this->database->rollBack();
				return UPDATE_ERROR;
			}
		}else{
			Util::dd($this->getValidator()->errors()->all());
			return VALIDATION_ERROR;
		}
	}
	public function getAddressByCustomerId($id){
		$query = "select address.* from customers INNER JOIN address_customer INNER JOIN address ON customers.id = address_customer.customer_id and address_customer.address_id=address.id and customers.id=".$id;
		$result = $this->di->get("database")->raw($query);
		return $result[0];
	}
	public function getCustomerById($id){
		// $result = $this->di->get("database")->readData("customers",[],"id=".$id);
		$query = "select address.*,customers.* from customers INNER JOIN address_customer INNER JOIN address ON customers.id = address_customer.customer_id and address_customer.address_id=address.id and customers.id=".$id;
		$result = $this->di->get("database")->raw($query);
		// Util::dd($result);
		return $result[0];
	}
	public function addCustomer($data)
	{
		//validate data
		$this->validateData($data);
		//insert data in database
		if(!$this->validator->fails())
		{
			try
			{
				$this->database->beginTransaction();
				$data_to_be_inserted = [
					'first_name'=>$data['first_name'],
					'last_name'=>$data['last_name'],
					'gst_no'=>$data['gst_no'],
					'phone_no'=>$data['phone_no'],
					'email_id'=>$data['email_id'],
					'gender'=>$data['gender'],
				];
				$customer_id = $this->database->insert($this->table,$data_to_be_inserted);

				$data_to_be_inserted = [
					'block_no'=>$data['block_no'],
					'street'=>$data['street'],
					'city'=>$data['city'],
					'pincode'=>$data['pincode'],
					'state'=>$data['state'],
					'country'=>$data['country'],
					'town'=>$data['town'],
				];
				$address_id = $this->database->insert("address",$data_to_be_inserted);
				
				$data_to_be_inserted = [
					'address_id'=>$address_id,
					'customer_id'=>$customer_id	
				];
				$address_customer = $this->database->insert("address_customer",$data_to_be_inserted);

				$this->database->commit();
				return ADD_SUCCESS;
			}catch(Exception $e){
				$this->database->rollBack();
				return ADD_ERROR;
			}
		}
		return VALIDATION_ERROR;
	}
	public function delete($id)
    {
        try{
            $this->database->beginTransaction();
			$this->database->delete($this->table, "id = {$id}");

			$address = $this->getAddressByCustomerId($id);
			// Util::dd($address->id);
			$this->database->delete("address", "id = {$address->id}");
			$this->database->commit();
            return DELETE_SUCCESS;
        }catch(Exception $e){
            $this->database->rollBack();
            return DELETE_ERROR;
        }
	}
	public function getCustomerIdByEmail($email){
		// $result = $this->database->raw("select id from customers where email_id='{$email}'");
		$result = $this->database->readData("customers",["id"],"email_id='{$email}'");
		// Util::dd($result);
		if($this->database->count()<1){
			return false;
		}
		return $result[0];
	}
	public function getJSONDataForDataTable($draw,$search_parameter,$order_by,$start,$length)
	{
		// $query = "select CONCAT(block_no, \", \", street, \", \", city, \" - \", pincode, \".\" , \" \", state, \".\", \" \", country, \".\", \" \", town) as full_address,suppliers.* from address INNER JOIN address_supplier inner join suppliers on address.id=address_supplier.address_id and address_supplier.supplier_id = suppliers.id and address.id in (SELECT address_supplier.address_id from address_supplier INNER JOIN suppliers ON suppliers.id=address_supplier.supplier_id) and suppliers.deleted=0";
		$query = "select CONCAT(block_no, \" , \", street,\" , \", city,\" - \", pincode ,\" , \", state,\" , \", country,\" , \",town) as full_address,customers.* from address INNER JOIN address_customer inner join customers on address.id=address_customer.address_id and address_customer.customer_id = customers.id and customers.id in (SELECT address_customer.address_id from address_customer INNER JOIN customers ON customers.id=address_customer.customer_id) and customers.deleted=0";

		$totalRowCountQuery = "select count(*) as total_count from address INNER JOIN address_customer inner join customers on address.id=address_customer.address_id and address_customer.customer_id = customers.id and address.id in (SELECT address_customer.address_id from address_customer INNER JOIN customers ON customers.id=address_customer.customer_id) and customers.deleted=0";
		$filteredRowCountQuery = "select count(*) as total_count from address INNER JOIN address_customer inner join customers on address.id=address_customer.address_id and address_customer.customer_id = customers.id and address.id in (SELECT address_customer.address_id from address_customer INNER JOIN customers ON customers.id=address_customer.customer_id) and customers.deleted=0";

        
		if($search_parameter != null)
		{
			$query .= " AND customers.first_name like '%{$search_parameter}%' OR customers.last_name like '%{$search_parameter}%'";
			$filteredRowCountQuery .= " AND customers.first_name LIKE '%{$search_parameter}%' OR customers.last_name like '%{$search_parameter}%'";
		}

		if($order_by != null)
		{
			$query .= " ORDER BY customers.{$this->columns[$order_by[0]['column']]} {$order_by[0]['dir']}";
			$filteredRowCountQuery .=" ORDER BY customers.{$this->columns[$order_by[0]['column']]} {$order_by[0]['dir']}";
		}
		else{
			$query .= " ORDER BY customers.{$this->columns[0]} ASC";
			$filteredRowCountQuery.=" ORDER BY customers.{$this->columns[0]} ASC";
		}

		if($length != -1)
		{
			$query .= " LIMIT {$start}, {$length}";
		}
		// Util::dd($query);

		$totalRowCountResult = $this->database->raw($totalRowCountQuery);
		$numberOfTotalRows = is_array($totalRowCountResult) ? $totalRowCountResult[0]->total_count : 0;

		$filteredRowCountResult = $this->database->raw($filteredRowCountQuery);
		$numberOfFilteredRows = is_array($filteredRowCountResult) ? $filteredRowCountResult[0]->total_count : 0;
		// Util::dd($filteredRowCountResult);
		$fetchData = $this->database->raw($query);
        $data = [];
		$numRows = is_array($fetchData) ? count($fetchData) : 0;
		for($i=0;$i<$numRows;$i++)
		{
			$subArray = [];
			$subArray[] = $start+$i+1;
            $subArray[] = $fetchData[$i]->first_name;
            $subArray[] = $fetchData[$i]->last_name;
            $subArray[] = $fetchData[$i]->gst_no;
            $subArray[] = $fetchData[$i]->phone_no;
            $subArray[] = $fetchData[$i]->email_id;
            $subArray[] = $fetchData[$i]->gender;
            $subArray[] = $fetchData[$i]->created_at;
			$subArray[] = $fetchData[$i]->updated_at;
			$subArray[] = $fetchData[$i]->full_address;
			$subArray[] = <<<BUTTONS
<a href='edit-customer.php?id={$fetchData[$i]->id}'><button class='btn btn-outline-primary btn-sm edit' data-id='{$fetchData[$i]->id}' data-toggle="modal" data-target="#editModal"><i class="fas fa-pencil-alt"></i></button></a>
<button class='btn btn-outline-danger btn-sm delete' data-id='{$fetchData[$i]->id}' data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash-alt"></i></button>
BUTTONS;
            $data[] = $subArray;
		}
		$output = array(
			'draw'=>$draw,
			'recordsTotal'=>$numberOfTotalRows,
			'recordsFiltered'=>$numberOfFilteredRows,
			'data'=>$data,
		);
		
		echo json_encode($output);
	}
}