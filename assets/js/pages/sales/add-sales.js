var id = 2;

function deleteProduct(delete_id) {
    var elements = document.getElementsByClassName("product_row");
    if (elements.length != 1) {
        $("#element_" + delete_id).remove();
    }
    final_rate_calculator();
}

function addProduct() {
    $("#products_container").append(`<!--BEGIN: PRODUCT CUSTOM CONTROL-->
    <div class="row product_row" id="element_${id}">
        <!--BEGIN: CATEGORY SELECT-->
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Category</label>
                <select id="category_${id}" class="form-control category_select">
                    <option disabled selected>Select Category</option>
                </select>
            </div>
        </div>
        <!--END: CATEGORY SELECT-->

        <!--BEGIN: PRODUCT SELECT-->
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Products</label>
                <select name="product_id[]" id="product_${id}" class="form-control product_select">
                    <option disabled selected>Select Product</option>
                </select>
            </div>
        </div>
        <!--END: PRODUCT SELECT-->

        <!--BEGIN: SELLING PRICE-->
        <div class="col-md-2">
            <div class="form-group">
               <label for="">Selling Price</label>
               <input type="text" id="selling_price_${id}"
                       class="form-control" disabled>
            </div>
        </div>
        <!--END: SELLING PRICE-->
        <!--BEGIN: QUANTITY-->
        <div class="col-md-1">
            <div class="form-group">
               <label for="">Quantity</label>
               <input type="text" name="quantity[]" id="quantity_${id}"
                       class="form-control quantity_input"
                       value="1" onkeypress="numericInputField(event)">
            </div>
        </div>
        <!--END: QUANTITY-->
        <!--BEGIN: DISCOUNT-->
        <div class="col-md-1">
            <div class="form-group">
               <label for="">Discount</label>
               <input type="text" name="discount[]" id="discount_${id}"
                       class="form-control discount_input"
                       value="1" onkeypress="numericInputField(event)">
            </div>
        </div>
        <!--END: final rate-->
        <!--BEGIN: Final Rate PRICE-->
        <div class="col-md-2">
            <div class="form-group">
               <label for="">Final Rate</label>
               <input type="text" name="final_rate[]" id="final_rate_${id}"
                       class="form-control final_rate"
                       value="0" readonly>
            </div>
        </div>
        <!--END: Final Rate PRICE-->

        <!--BEGIN: DELETE BUTTON-->
        <div class="col-md-1">
            <button onclick="deleteProduct(${id})" type="button" class="btn btn-danger" style="margin-top: 40%">
                <i class="far fa-trash-alt"></i>
            </button>
        </div>
        <!--END: DELETE BUTTON-->

    </div>
    <!--END: PRODUCT CUSTOM CONTROL-->`);

    var baseURL = window.location.origin;
    var filePath = "/helper/routing.php";
    $.ajax({
        url: baseURL + filePath,
        method: 'POST',
        data: {
            getCategories: true
        },
        dataType: 'json',
        success: function(categories) {
            categories.forEach(function(category) {
                $("#category_" + id).append(
                    `<option value='${category.id}'>${category.name}</option>`
                );
            });
            id++;
        }
    });
}

//using event delegation
// START EVENTS
$("#products_container").on('change', '.category_select', function() {
    var element_id = $(this).attr('id').split("_")[1];
    var category_id = this.value;
    var baseURL = window.location.origin;
    var filePath = "/helper/routing.php";
    $.ajax({
        url: baseURL + filePath,
        method: 'POST',
        data: {
            getProductsByCategoryID: true,
            categoryID: category_id
        },
        dataType: 'json',
        success: function(products) {
            $("#product_" + element_id).empty();
            $("#product_" + element_id).append("<option disabled selected>Select Product</option>");
            products.forEach(function(product) {
                $("#product_" + element_id).append(
                    `<option value='${product.id}'>${product.name}</option>`
                );
            });
        }
    })
});

$("#products_container").on('change', '.product_select', function() {
    var element_id = $(this).attr('id').split("_")[1];
    var product_id = this.value;
    $("#quantity_" + element_id).val(1);
    $("#discount_" + element_id).val(0);

    var baseURL = window.location.origin;
    var filePath = "/helper/routing.php";
    $.ajax({
        url: baseURL + filePath,
        method: 'POST',
        data: {
            getSellingPriceByProductID: true,
            productID: product_id
        },
        dataType: 'json',
        success: function(sellingRate) {
            $("#selling_price_" + element_id).empty();
            $("#selling_price_" + element_id).val(sellingRate[0].selling_rate);
            $("#final_rate_" + element_id).val(sellingRate[0].selling_rate);
            final_rate_calculator();
        }
    })
});

$("#products_container").on('keyup', '.quantity_input', function() {
    var element_id = $(this).attr('id').split("_")[1];
    var quantity = $(this).attr('id');
    var value = $(this).val();
    var selling_price = $("#selling_price_" + element_id).val();
    var discount = $("#discount_" + element_id).val();
    var finalRate = value * selling_price;
    var discountRate = finalRate * discount / 100;

    finalRate -= discountRate;
    $("#final_rate_" + element_id).val(finalRate);
    final_rate_calculator();
});

$("#products_container").on('keyup', '.discount_input', function() {
    var element_id = $(this).attr('id').split("_")[1];
    var value = $(this).val();
    var quantity = $("#quantity_" + element_id).val();
    var selling_price = $("#selling_price_" + element_id).val();
    var finalRate = quantity * selling_price;
    var discountRate = finalRate * value / 100;

    finalRate -= discountRate;
    $("#final_rate_" + element_id).val(finalRate);

    demo = $(".final_rate").val();

    final_rate_calculator();
});

$("#check_email").on('click', function(e) {
    var email = $("#customer_email").val();
    var baseURL = window.location.origin;
    var filePath = "/helper/routing.php";
    $.ajax({
        url: baseURL + filePath,
        method: 'POST',
        data: {
            getCustomerIdByEmail: true,
            email_id: email
        },
        dataType: 'json',
        success: function(result) {
            $("#email_verify_fail").css("display", "none");
            $("#email_verify_fail").removeClass("d-inline-block");
            $("#email_verify_success").css("display", "none");
            $("#email_verify_success").removeClass("d-inline-block");
            if (result == false) {
                // alert("false");
                $("#email_verify_fail").css("display", "block");
                $("#email_verify_fail").addClass("d-inline-block");
                $("#disable").addClass("disable-sec");
                $("#customer_id").val("");
            } else {
                $("#email_verify_success").css("display", "block");
                $("#email_verify_success").addClass("d-inline-block");
                $("#customer_id").val(result.id);
                $("#disable").removeClass("disable-sec");
                // alert("not false");
            }
        }
    })
});
// !END EVENTS

// supprting functions 
function numericInputField(event) {
    var x = event.keyCode;
    if (!(x > 46 && x <= 57)) {
        event.preventDefault();
    }
}

function final_rate_calculator() {
    elements = document.getElementsByClassName("final_rate");
    var final_total_amount = 0;
    // console.log(elements.length);
    for (i = 0; i < elements.length; i++) {
        // console.log(elements[i].value);
        integer_value = parseInt(elements[i].value);
        final_total_amount += integer_value;
    }
    $("#finalTotal").val(final_total_amount);
}