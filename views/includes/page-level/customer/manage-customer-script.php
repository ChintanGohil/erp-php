<script src="<?=BASEASSETS;?>js/plugins/toastr/toaster.min.js"></script>
<script src="<?=BASEASSETS;?>vendor/datatables/dataTables.min.js"></script>
<script src="<?=BASEASSETS;?>js/pages/customer/manage-customer.js"></script>
<script>
	toastr.options = {
	  "closeButton": false,
	  "debug": false,
	  "newestOnTop": false,
	  "progressBar": false,
	  "positionClass": "toast-top-right",
	  "preventDuplicates": false,
	  "onclick": null,
	  "showDuration": "300",
	  "hideDuration": "1000",
	  "timeOut": "5000",
	  "extendedTimeOut": "1000",
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "fadeIn",
	  "hideMethod": "fadeOut"
	}
	<?php
		if(Session::hasSession(ADD_SUCCESS)):
	?>
			toastr.success("New customer has been added successfully");
	<?php
			Session::unsetSession(ADD_SUCCESS);
		elseif(Session::hasSession(ADD_ERROR)):
	?>
			toastr.error("Adding a record failed");
	<?php
			Session::unsetSession(ADD_ERROR);
		elseif(Session::hasSession(EDIT_SUCCESS)):
	?>
			toastr.success("A record has been edited successfully");
	<?php
			Session::unsetSession(EDIT_SUCCESS);
		elseif(Session::hasSession(EDIT_ERROR)):
	?>
		toastr.error("Editing a record is failed");
	<?php
			Session::unsetSession(EDIT_ERROR);

		elseif(Session::hasSession(DELETE_SUCCESS)):
	?>
		toastr.success("Record deleted successfully");
	<?php
			Session::unsetSession(DELETE_SUCCESS);
		elseif(Session::hasSession(DELETE_ERROR)):
	?>
		toastr.error("Deleting a record is failed");
	<?php
			Session::unsetSession(DELETE_ERROR);

		elseif(Session::hasSession('csrf')):
	?>
			toastr.error("Unauthorized Access,Token match","Token error!");
	<?php
			Session::unsetSession('csrf');
		endif;
	?>
</script>