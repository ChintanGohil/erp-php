<?php
require_once __DIR__ . "/../../helper/init.php";
$page_title = "Quick ERP | Add New Sales";
$sidebarSection = 'transaction';
$sidebarSubSection = 'purchase';

$data = $di->get("database")->raw("SELECT sales.*,customers.* from sales INNER JOIN invoice INNER JOIN customers ON invoice.id = sales.invoice_id and customers.id = invoice.customer_id and invoice.id={$_GET["invoice_id"]}");
Util::dd($data);

Util::createCSRFToken();
// Util::dd(Session::getSession('csrf_token'));
$errors = "";
$old = "";
if(Session::hasSession('old'))
{
  $old = Session::getSession('old');
  Session::unsetSession('old');
}

if(Session::hasSession('errors'))
{
  $errors = unserialize(Session::getSession('errors'));
  Session::unsetSession('errors');
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

<?php
    require_once __DIR__."/../includes/head-section.php";
?>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php
        require_once __DIR__."/../includes/sidebar.php";
    ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php  require_once __DIR__ ."/../includes/navbar.php";?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        


        
        <!-- /.container-fluid -->

      <!-- End of Main Content -->

      <!-- Footer -->
      <?php
                require_once __DIR__."/../includes/footer.php";
          ?> 
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->

  <!-- Logout Modal-->
  
  <?php require_once __DIR__."/../includes/core-scripts.php"; ?> 

  <script src="<?=BASEASSETS;?>js/plugins/jquery-validation/jquery.validate.min.js"></script>
  <script src="<?=BASEASSETS;?>js/pages/sales/add-sales.js"></script>
  
</body>

</html>
