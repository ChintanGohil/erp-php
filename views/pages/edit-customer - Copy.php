<?php
require_once __DIR__ . "/../../helper/init.php";
$page_title = "Quick ERP | Edit Customer";
$sidebarSection = 'customer';
$sidebarSubSection = 'manage';

Util::createCSRFToken();
// Util::dd(Session::getSession('csrf_token'));
$errors = "";
$old = "";
if(Session::hasSession('old'))
{
  $old = Session::getSession('old');
  Session::unsetSession('old');
}

if(Session::hasSession('errors'))
{
  $errors = unserialize(Session::getSession('errors'));
  Session::unsetSession('errors');
}

if(Session::hasSession(EDIT_CUSTOMER))
{
  Session::unsetSession(EDIT_CUSTOMER);
}

if(isset($_POST))
{
    $first_name = $_POST['first_name'];
    // Util::dd($_POST['first_name']);
    $last_name = $_POST['last_name'];
    $gst_no = $_POST['gst_no'];
    $phone_no = $_POST['phone_no'];
    $email_id = $_POST['email_id'];
    $gender = $_POST['gender'];
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

<?php
    require_once __DIR__."/../includes/head-section.php";
?>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php
        require_once __DIR__."/../includes/sidebar.php";
    ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
          <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            <!-- Nav Item - Alerts -->
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                <!-- Counter - Alerts -->
                <span class="badge badge-danger badge-counter">3+</span>
              </a>
              <!-- Dropdown - Alerts -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                  Alerts Center
                </h6>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-primary">
                      <i class="fas fa-file-alt text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 12, 2019</div>
                    <span class="font-weight-bold">A new monthly report is ready to download!</span>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-success">
                      <i class="fas fa-donate text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 7, 2019</div>
                    $290.29 has been deposited into your account!
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-warning">
                      <i class="fas fa-exclamation-triangle text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 2, 2019</div>
                    Spending Alert: We've noticed unusually high spending for your account.
                  </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
              </div>
            </li>

            <!-- Nav Item - Messages -->
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-envelope fa-fw"></i>
                <!-- Counter - Messages -->
                <span class="badge badge-danger badge-counter">7</span>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                <h6 class="dropdown-header">
                  Message Center
                </h6>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60" alt="">
                    <div class="status-indicator bg-success"></div>
                  </div>
                  <div class="font-weight-bold">
                    <div class="text-truncate">Hi there! I am wondering if you can help me with a problem I've been having.</div>
                    <div class="small text-gray-500">Emily Fowler · 58m</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/AU4VPcFN4LE/60x60" alt="">
                    <div class="status-indicator"></div>
                  </div>
                  <div>
                    <div class="text-truncate">I have the photos that you ordered last month, how would you like them sent to you?</div>
                    <div class="small text-gray-500">Jae Chun · 1d</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/CS2uCrpNzJY/60x60" alt="">
                    <div class="status-indicator bg-warning"></div>
                  </div>
                  <div>
                    <div class="text-truncate">Last month's report looks great, I am very happy with the progress so far, keep up the good work!</div>
                    <div class="small text-gray-500">Morgan Alvarez · 2d</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60" alt="">
                    <div class="status-indicator bg-success"></div>
                  </div>
                  <div>
                    <div class="text-truncate">Am I a good boy? The reason I ask is because someone told me that people say this to all dogs, even if they aren't good...</div>
                    <div class="small text-gray-500">Chicken the Dog · 2w</div>
                  </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
              </div>
            </li>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Valerie Luna</span>
                <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Settings
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Activity Log
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between">
            <h1 class="h3 b-4 text-gray-800">Edit Customer</h1>
            <a href="" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
              <i class="fas fa-list-ul fa-sm text-white"></i> Manage Customer
            </a>
          </div>

          <div class="container-fluid">
              <div class="row">
                <div class="col-md-12">
                  <div class="card shadow mb-4">
                    <!-- CARD HEADER -->
                    <div class="card-header">
                      <h6 class="m-0 font-weight-bold text-primary">
                        <i class="fa fa-plus"></i> Edit Customer
                      </h6>
                    </div>
                    <!-- END OF CARD HEADER -->

                    <!-- CARD BODY -->
                    <div class="card-body">
                        <form action="<?= BASEURL?>helper/routing.php" method="POST" id="edit-customer">
                          <input type="hidden" name="csrf_token" value=<?= Session::getSession('csrf_token'); ?>>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="first_name">First Name</label>
                                        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('first_name') ? 'error is-invalid' : '') : ''; ?>" name="first_name" id="first_name" placeholder="Enter First Name" value="<?= $first_name;?>">
                                      <?php
                                      
                                      if($errors!="" && $errors->has('first_name')):
                                          echo "<span class='error'> {$errors->first('first_name')}</span>";
                                      endif;
                                      
                                      ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="last_name">Last Name</label>
                                        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('last_name') ? 'error is-invalid' : '') : ''; ?>" name="last_name" id="last_name" placeholder="Enter Last Name" value="<?= $last_name;?>">
                                      <?php
                                      
                                      if($errors!="" && $errors->has('last_name')):
                                          echo "<span class='error'> {$errors->first('last_name')}</span>";
                                      endif;
                                      
                                      ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="gst_no">GST Number</label>
                                        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('gst_no') ? 'error is-invalid' : '') : ''; ?>" name="gst_no" id="gst_no" placeholder="Enter GST Number" value="<?= $gst_no;?>">
                                      <?php
                                      
                                      if($errors!="" && $errors->has('gst_no')):
                                          echo "<span class='error'> {$errors->first('gst_no')}</span>";
                                      endif;
                                      
                                      ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="phone_no">Phone Number</label>
                                        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('phone_no') ? 'error is-invalid' : '') : ''; ?>" name="phone_no" id="phone_no" placeholder="Enter Phone Number" value="<?= $phone_no;?>">
                                      <?php
                                      
                                      if($errors!="" && $errors->has('phone_no')):
                                          echo "<span class='error'> {$errors->first('phone_no')}</span>";
                                      endif;
                            
                                      ?>
                                    </div>
                                </div>
                                
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="email_id">Email ID</label>
                                        <input type="text" class="form-control <?= $errors!='' ? ($errors->has('email_id') ? 'error is-invalid' : '') : ''; ?>" name="email_id" id="email_id" placeholder="Enter Email ID" value="<?= $email_id;?>">
                                      <?php
                                      
                                      if($errors!="" && $errors->has('email_id')):
                                          echo "<span class='error'> {$errors->first('email_id')}</span>";
                                      endif;
                            
                                      ?>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="gender">Gender</label>
                                        <br>
                                        <div class="btn-group btn-group-toggle" id="gender" data-toggle="buttons">
                                            <label class="btn btn-primary <?= $gender == 'Male' ? 'active' : '';?>">
                                                <input type="radio" name="gender" id="male" value="Male" autocomplete="off" checked> Male
                                            </label>
                                            <label class="btn btn-primary <?= $gender == 'Female' ? 'active' : '';?>">
                                                <input type="radio" name="gender" id="female" value="Female" autocomplete="off"> Female
                                            </label>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <input type="submit" class="btn btn-primary" name="add_customer" value="Submit">
                        </form>
                    </div>
                    <!-- END OF CARD BODY -->    
                    
                  </div>
                </div>
              </div>
          </div>


        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php
                require_once __DIR__."/../includes/footer.php";
          ?> 
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <?php require_once __DIR__."/../includes/scroll-to-top.php"; ?> 

  <!-- Logout Modal-->
  
  <?php require_once __DIR__."/../includes/core-scripts.php"; ?> 

  <script src="<?=BASEASSETS;?>js/plugins/jquery-validation/jquery.validate.min.js"></script>
  <!-- <script src="<?=BASEASSETS;?>js/pages/customer/manage-customer.js"></script> -->
  
</body>

</html>
