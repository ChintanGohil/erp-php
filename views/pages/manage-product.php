<?php
require_once __DIR__."/../../helper/init.php";
$page_title = "Quick ERP | Manage Product";
$sidebarSection = 'product';
$sidebarSubSection = 'manage-product';

$sidebarTitle = "Quick ERP";
?>
<!DOCTYPE html>
<html lang="en">

<head>
  
  <?php
  require_once __DIR__."/../includes/head-section.php";
  ?>
  <link rel="stylesheet" type="text/css" href="<?=BASEASSETS;?>css/plugins/toastr/toaster.min.css">
  <link rel="stylesheet" type="text/css" href="<?=BASEASSETS;?>vendor/datatables/datatables.min.css">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once __DIR__."/../includes/sidebar.php" ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php require_once __DIR__."/../includes/navbar.php" ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <h1 class="h3 mb-4 text-gray-888">Manage Product</h1>
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Products</h6>
                </div>
                <div class="card-body">
                    <div id="export-buttons"></div>
                    <table class="table table-bordered table-responsive" id="manage-product-table">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Product Name</th>
                            <th>Specifications</th>
                            <th>Selling Rate</th>
                            <th>WEF</th>
                            <th>Category</th>
                            <th>EOQ</th>
                            <th>Danger Level</th>
                            <th>Supplier Names</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->

        <!-- /.container-fluid -->

      <!-- Footer -->
      <?php
      require_once __DIR__."/../includes/footer.php";
      ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <?php
  require_once __DIR__."/../includes/scroll-top.php";
  ?>

  <!-- Logout Modal-->
  

  <?php
  require_once __DIR__."/../includes/core-scripts.php";
  ?>
  <?php
  require_once __DIR__."/../includes/index-scripts.php";
  ?>
  <?php
  require_once __DIR__."/../includes/page-level/product/manage-product-scripts.php";
  ?>
  

</body>

</html>
