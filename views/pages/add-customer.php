<?php
require_once __DIR__."/../../helper/init.php";
$page_title = "Quick ERP | Add New Catagory";
$sidebarSection = 'customer';
$sidebarSubSection = 'add-customer';
Util::createCSRFToken();
$errors = "";
$old = "";
if(Session::hasSession('old'))
{

  $old = Session::getSession('old');
  Session::unsetSession('old');
}
if(Session::hasSession('errors'))
{
  // Util::dd(Session::getSession('errors'));
  $errors = unserialize(Session::getSession('errors'));
  Session::unsetSession('errors');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  
  <?php
  require_once __DIR__."/../includes/head-section.php";
  ?>
  

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
      <?php require_once __DIR__."/../includes/sidebar.php" ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php require_once __DIR__."/../includes/navbar.php" ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
          <div class="d-sm-flex align-items-center justify-content-between">
            <h1 class="h3 mb-4 text-gray-888">Add Customer
            </h1>
            <a href="manage-customer.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
              <i class="fas fa-list-ul fa-sm text-white"></i>Manage Customer
            </a>
          </div>
          
        </div>

        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card shadow mb-4">
                <div class="card-header">
                  <h6 class="m-0 font-weight-bold text-primary">
                    <i class="fa fa-plus"></i>Add Customer
                    
                  </h6>
                </div>
 
                <!--CARD BODY-->
                <div class="card-body">
                  <form action="<?=BASEURL;?>helper/routing.php" method="POST" id="add-customer">
                    <input type="hidden" name="csrf_token" value="<?= Session::getSession('csrf_token');?>">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">

                        <label class="m-t-2">First Name</label>
                        <input type="username" class="form-control
                        <?=$errors!='' ? ($errors->has('first_name') ? 
                        'error is-invalid' : '') : '';?>" name="first_name" id="first_name" placeholder="Enter First Name" value = "<?=$old!= '' ? $old['first_name']: '';?>">
                        <?php
                        if($errors!="" && $errors->has('first_name')):
                          echo "<span class='error'>{$errors->first('first_name')}</span>";
                        endif;
                        ?>
                            
                        <label class="mt-3">Last Name</label>
                        <input type="text" class="form-control 
                        <?=$errors!='' ? ($errors->has('last_name') ? 
                        'error is-invalid' : '') : '';?>" name="last_name" id="last_name" placeholder="Enter Last Name" value = "<?=$old!= '' ? $old['last_name']: '';?>">
                        <?php
                        if($errors!="" && $errors->has('last_name')):
                          echo "<span class='error'>{$errors->first('last_name')}</span>";
                        endif;
                        ?>

                        <label class="mt-3">GST No</label>
                        <input type="text" class="form-control 
                        <?=$errors!='' ? ($errors->has('gst_no') ? 
                          'error is-invalid' : '') : '';?>" name="gst_no" id="gst_no" placeholder="Enter GST No" value = "<?=$old!= '' ? $old['gst_no']: '';?>">
                        <?php
                        if($errors!="" && $errors->has('gst_no')):
                          echo "<span class='error'>{$errors->first('gst_no')}</span>";
                        endif;
                        ?>

                        <label class="mt-3">Phone No</label>
                        <input type="text" class="form-control 
                        <?=$errors!='' ? ($errors->has('phone_no') ? 
                          'error is-invalid' : '') : '';?>" name="phone_no" id="phone_no" placeholder="Enter Phone No" value = "<?=$old!= '' ? $old['phone_no']: '';?>">
                        <?php
                            if($errors!="" && $errors->has('phone_no')):
                                echo "<span class='error'>{$errors->first('phone_no')}</span>";
                            endif;
                        ?>

                        <label class="mt-3">Email</label>
                        <input type="email" class="form-control 
                        <?=$errors!='' ? ($errors->has('email_id') ? 
                          'error is-invalid' : '') : '';?>" name="email_id" id="email" placeholder="Enter Email" value = "<?=$old!= '' ? $old['email_id']: '';?>">
                        <?php
                            if($errors!="" && $errors->has('email_id')):
                                echo "<span class='error'>{$errors->first('email_id')}</span>";
                            endif;
                        ?>

                        <label class="mt-3">Gender</label>
                        <input type="text" class="form-control 
                        <?=$errors!='' ? ($errors->has('gender') ? 
                          'error is-invalid' : '') : '';?>" name="gender" id="gender" placeholder="Enter Gender" value = "<?=$old!= '' ? $old['gender']: '';?>">
                        <?php
                            if($errors!="" && $errors->has('gender')):
                                echo "<span class='error'>{$errors->first('gender')}</span>";
                            endif;
                        ?>

                        <label class="mt-3">Block_no</label>
                        <input type="text" class="form-control 
                        <?=$errors!='' ? ($errors->has('block_no') ? 
                          'error is-invalid' : '') : '';?>" name="block_no" id="block_no" placeholder="Enter block no" value = "<?=$old!= '' ? $old['block_no']: '';?>">
                        <?php
                            if($errors!="" && $errors->has('block_no')):
                                echo "<span class='error'>{$errors->first('block_no')}</span>";
                            endif;
                        ?>

                        <label class="mt-3">Street</label>
                        <input type="text" class="form-control 
                        <?=$errors!='' ? ($errors->has('street') ? 
                          'error is-invalid' : '') : '';?>" name="street" id="street" placeholder="Enter street" value = "<?=$old!= '' ? $old['street']: '';?>">
                        <?php
                            if($errors!="" && $errors->has('street')):
                                echo "<span class='error'>{$errors->first('street')}</span>";
                            endif;
                        ?>                       

                        <label class="mt-3">City</label>
                        <input type="text" class="form-control 
                        <?=$errors!='' ? ($errors->has('city') ? 
                          'error is-invalid' : '') : '';?>" name="city" id="city" placeholder="Enter City" value = "<?=$old!= '' ? $old['city']: '';?>">
                        <?php
                            if($errors!="" && $errors->has('city')):
                                echo "<span class='error'>{$errors->first('city')}</span>";
                            endif;
                        ?>

                        <label class="mt-3">Pincode</label>
                        <input type="text" class="form-control 
                        <?=$errors!='' ? ($errors->has('pincode') ? 
                          'error is-invalid' : '') : '';?>" name="pincode" id="pincode" placeholder="Enter pincode" value = "<?=$old!= '' ? $old['pincode']: '';?>">
                        <?php
                            if($errors!="" && $errors->has('pincode')):
                                echo "<span class='error'>{$errors->first('pincode')}</span>";
                            endif;
                        ?>

                        <label class="mt-3">State</label>
                        <input type="text" class="form-control 
                        <?=$errors!='' ? ($errors->has('state') ? 
                          'error is-invalid' : '') : '';?>" name="state" id="state" placeholder="Enter state" value = "<?=$old!= '' ? $old['state']: '';?>">
                        <?php
                            if($errors!="" && $errors->has('state')):
                                echo "<span class='error'>{$errors->first('state')}</span>";
                            endif;
                        ?>
                        
                        <label class="mt-3">Country</label>
                        <input type="text" class="form-control 
                        <?=$errors!='' ? ($errors->has('country') ? 
                          'error is-invalid' : '') : '';?>" name="country" id="country" placeholder="Enter country" value = "<?=$old!= '' ? $old['country']: '';?>">
                        <?php
                            if($errors!="" && $errors->has('country')):
                                echo "<span class='error'>{$errors->first('country')}</span>";
                            endif;
                        ?>
                        
                        <label class="mt-3">Town</label>
                        <input type="text" class="form-control 
                        <?=$errors!='' ? ($errors->has('town') ? 
                          'error is-invalid' : '') : '';?>" name="town" id="town" placeholder="Enter town" value = "<?=$old!= '' ? $old['town']: '';?>">
                        <?php
                            if($errors!="" && $errors->has('town')):
                                echo "<span class='error'>{$errors->first('town')}</span>";
                            endif;
                        ?>                        
                        </div>
                      </div>
                    </div>
                    <input type="submit" name="add_customer" class="btn btn-primary" value="Submit">
                  </form>
                </div>
                <!--/CARD BODY-->

              </div>
            </div>
          </div>
          
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php
      require_once __DIR__."/../includes/footer.php";
      ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <?php
  require_once __DIR__."/../includes/scroll-top.php";
  ?>

  <!-- Logout Modal-->
  

  <?php
  require_once __DIR__."/../includes/core-scripts.php";
  ?>
  <?php
  require_once __DIR__."/../includes/index-scripts.php";
  ?>
  <script src="<?=BASEASSETS;?>js/plugins/jquery-validation/jquery.validate.js"></script>
  <script src="<?=BASEASSETS;?>js/pages/customer/add-customer.js"></script>

  

</body>

</html>
